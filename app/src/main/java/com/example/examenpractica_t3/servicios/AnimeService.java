package com.example.examenpractica_t3.servicios;

import com.example.examenpractica_t3.personaje.Anime;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AnimeService {
    @GET("/listaAnimes")
    Call<List<Anime>> all();

}