package com.example.examenpractica_t3.personaje;

import com.google.gson.annotations.SerializedName;

public class Anime {
    @SerializedName("Nombre")
    private String nombre;
    @SerializedName("Descripcion")
    private String descripcion;
    @SerializedName("Foto")
    private String url_imagen;

    public Anime() {
    }

    public Anime(String nombre, String descripcion, String url_imagen) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.url_imagen = url_imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrl_imagen() {
        return url_imagen;
    }

    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }
}
